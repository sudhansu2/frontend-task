import LogsTypes from "./types";

export const fetchLogsStart = () => {
  return {
    type: LogsTypes.FETCH_LOGS_START,
  };
};

export const fetchLogsSuccess = (data) => {
  return {
    type: LogsTypes.FETCH_LOGS_SUCCES,
    payload: data,
  };
};

export const fetchLogsFailure = (errorMessage) => {
  return {
    type: LogsTypes.FETCH_LOGS_FAILURE,
    payload: errorMessage,
  };
};
