import LogsTypes from "./types";

const initialState = {
  isFetching: false,
  data: [],
  options: [],
  errorMessage: undefined,
};

const logsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LogsTypes.FETCH_LOGS_START:
      return {
        ...state,
        isFetching: true,
      };

    case LogsTypes.FETCH_LOGS_SUCCES:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
      };

    case LogsTypes.FETCH_LOGS_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
      };

    default:
      return state;
  }
};

export default logsReducer;
