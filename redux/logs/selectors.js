import { createSelector } from "reselect";
import { filterOptions, colHeader } from "@/utils/configs";

const selectLogs = (state) => {
  return state.data;
};

export const selectIsLogFetching = createSelector([selectLogs], (res) => {
  return res.isFetching;
});

export const selectIsLogOptions = createSelector([selectLogs], (res) => {
  let options = []; // filter options

  let auditLog = res?.data?.result?.auditLog;

  const uniqueActionTypeArray = auditLog
    .filter((value, index) => {
      const _actionTypeValue = value.actionType.toString();

      if (_actionTypeValue !== "null") {
        return (
          index ===
          auditLog.findIndex((obj) => {
            return obj.actionType.toString() === _actionTypeValue;
          })
        );
      }
    })
    .map((d) => {
      return {
        value: d.actionType,
        label: d.actionType,
      };
    });

  const uniqueApplicationTypeArray = auditLog
    .filter((value, index) => {
      const _applicationTypeValue = JSON.stringify(value.applicationType);
      if (_applicationTypeValue !== "null") {
        return (
          index ===
          auditLog.findIndex((obj) => {
            return (
              JSON.stringify(obj.applicationType) === _applicationTypeValue
            );
          })
        );
      }
    })
    .map((d) => ({
      value: d.applicationType,
      label: d.applicationType,
    }));

  options = filterOptions.map((d) => {
    if (d.name === "actionType") {
      return {
        ...d,
        options: uniqueActionTypeArray,
      };
    }

    if (d.name === "applicationType") {
      return {
        ...d,
        options: uniqueApplicationTypeArray,
      };
    }

    return {
      ...d,
    };
  });

  return options;
});

export const selectLogsItems = createSelector([selectLogs], (res) => {
  return res.data.result;
});
