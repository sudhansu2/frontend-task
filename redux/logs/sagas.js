import { takeLatest, call, put, all } from "redux-saga/effects";

import * as services from "../../services";
import { fetchLogsSuccess, fetchLogsFailure } from "./actions";
import LogsTypes from "./types";

export function* fetchLogsAsync() {
  try {
    const response = yield call(services.getLogs);
    yield put(fetchLogsSuccess(response.data));
  } catch (error) {
    yield put(fetchLogsFailure(error.message));
  }
}

export function* fetchLogsStart() {
  yield takeLatest(LogsTypes.FETCH_LOGS_START, fetchLogsAsync);
}

export function* logsSagas() {
  yield all([call(fetchLogsStart)]);
}
