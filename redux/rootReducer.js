import { combineReducers } from "redux";

import logsReducer from "./logs/reducer";

const rootReducer = combineReducers({
  data: logsReducer,
});

export default rootReducer;
