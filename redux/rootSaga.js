import { all, call } from "redux-saga/effects";

import { logsSagas } from "./logs/sagas";

export default function* rootSaga() {
  yield all([call(logsSagas)]);
}
