import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";

import "../styles/globals.css";

import createStore from "../redux/configureStore";

function MyApp({ Component, pageProps, store }: any) {
  return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }: any) => {
  let pageProps = {};

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps({ ctx });
  }

  return {
    pageProps: pageProps,
  };
};

export default withRedux(createStore)(MyApp);
