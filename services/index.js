import axios from "axios";

import * as config from "../utils/configs";

export const getLogs = () => {
  try {
    return axios.get(config.API_URL + config.LOG_ID);
  } catch (error) {
    console.error(error);
  }
};
